### Systemsicherheit

# Port Scans
Ein Port-Scan ist eine Methode zur Ermittlung, welche Ports in einem Netzwerk offen sind. Da Ports auf einem Computer der Ort sind, an dem Informationen gesendet und empfangen werden, ist das Port-Scannen analog zum Klopfen an Türen, um zu sehen, ob jemand zu Hause ist.

# Ip Spoofing
IP-Adress-Spoofing oder IP-Spoofing ist die Erstellung von Internet Protocol (IP)-Paketen mit einer falschen Quell-IP-Adresse, um sich als ein anderes Computersystem auszugeben.

Das Protokoll legt fest, dass jedes IP-Paket einen Header haben muss, der (u. a.) die IP-Adresse des Absenders des Pakets enthält. Die Quell-IP-Adresse ist normalerweise die Adresse, von der das Paket gesendet wurde, aber die Absenderadresse im Header kann verändert werden, so dass es für den Empfänger den Anschein hat, dass das Paket von einer anderen Quelle stammt.

Das Protokoll verlangt, dass der empfangende Computer eine Antwort an die Quell-IP-Adresse zurücksendet, so dass Spoofing hauptsächlich dann eingesetzt wird, wenn der Absender die Netzwerkantwort vorhersehen kann oder sich nicht um die Antwort kümmert.

Die Quell-IP-Adresse liefert nur begrenzte Informationen über den Absender. Sie kann allgemeine Informationen über die Region, die Stadt und den Ort liefern, an dem das Paket gesendet wurde. Sie liefert keine Informationen über die Identität des Absenders oder den verwendeten Computer.

IP-Adress-Spoofing, bei dem eine vertrauenswürdige IP-Adresse verwendet wird, kann von Eindringlingen im Netzwerk genutzt werden, um Sicherheitsmaßnahmen im Netzwerk zu überwinden, wie z. B. die Authentifizierung anhand von IP-Adressen. Diese Art von Angriff ist dort am effektivsten, wo Vertrauensbeziehungen zwischen Rechnern bestehen. In einigen Firmennetzwerken ist es beispielsweise üblich, dass sich interne Systeme gegenseitig vertrauen, so dass sich Benutzer ohne Benutzernamen oder Kennwort anmelden können, sofern sie sich von einem anderen Rechner im internen Netzwerk aus verbinden - was voraussetzen würde, dass sie bereits angemeldet sind. Durch das Spoofing einer Verbindung von einem vertrauenswürdigen Rechner aus kann ein Angreifer im selben Netzwerk ohne Authentifizierung auf den Zielrechner zugreifen.

IP-Adressen-Spoofing wird am häufigsten bei Denial-of-Service-Angriffen verwendet, bei denen das Ziel darin besteht, das Ziel mit einer überwältigenden Menge an Datenverkehr zu überfluten, und der Angreifer sich nicht darum kümmert, Antworten auf die Angriffspakete zu erhalten. Pakete mit gefälschten IP-Adressen sind schwieriger zu filtern, da jedes gefälschte Paket von einer anderen Adresse zu kommen scheint und sie die wahre Quelle des Angriffs verbergen. Denial-of-Service-Angriffe, die Spoofing verwenden, wählen typischerweise zufällig Adressen aus dem gesamten IP-Adressraum aus, obwohl ausgefeiltere Spoofing-Mechanismen möglicherweise nicht routbare Adressen oder ungenutzte Teile des IP-Adressraums vermeiden. Durch die Verbreitung großer Botnetze verliert Spoofing bei Denial-of-Service-Angriffen an Bedeutung, aber Angreifer haben Spoofing in der Regel als Werkzeug zur Verfügung, wenn sie es einsetzen wollen, sodass Abwehrmaßnahmen gegen Denial-of-Service-Angriffe, die sich auf die Gültigkeit der Quell-IP-Adresse in Angriffspaketen verlassen, Probleme mit gespooften Paketen haben könnten. Backscatter, eine Technik, die zur Beobachtung von Denial-of-Service-Angriffen im Internet verwendet wird, ist für ihre Effektivität auf die Verwendung von IP-Spoofing durch Angreifer angewiesen

# Defense against spoofing attacks
Die Paketfilterung ist eine Abwehrmaßnahme gegen IP-Spoofing-Angriffe. Das Gateway zu einem Netzwerk führt normalerweise eine Ingress-Filterung durch, d. h. das Blockieren von Paketen von außerhalb des Netzwerks mit einer Quelladresse innerhalb des Netzwerks. Dadurch wird verhindert, dass ein Angreifer von außen die Adresse eines internen Rechners fälscht. Im Idealfall führt das Gateway auch eine Egress-Filterung für ausgehende Pakete durch, d. h. es blockiert Pakete von innerhalb des Netzwerks mit einer Quelladresse, die nicht innerhalb des Netzwerks liegt. Dadurch wird verhindert, dass ein Angreifer innerhalb des Netzwerks, der die Filterung durchführt, IP-Spoofing-Angriffe gegen externe Rechner starten kann. Intrusion Detection System (IDS) ist eine gängige Anwendung der Paketfilterung, die zur Absicherung von Umgebungen für die gemeinsame Nutzung von Daten über das Netzwerk und hostbasierte IDS-Ansätze verwendet wurde.[citation needed]

Es wird auch empfohlen, Netzwerkprotokolle und -dienste so zu gestalten, dass sie sich nicht auf die Quell-IP-Adresse zur Authentifizierung verlassen.

# MAC - Spoofing
MAC-Spoofing ist eine Technik zum Ändern einer werkseitig zugewiesenen Media Access Control (MAC)-Adresse einer Netzwerkschnittstelle auf einem vernetzten Gerät. Die MAC-Adresse, die auf einem Netzwerkschnittstellen-Controller (NIC) fest codiert ist, kann nicht geändert werden. Viele Treiber erlauben es jedoch, die MAC-Adresse zu ändern. Außerdem gibt es Tools, die ein Betriebssystem dazu bringen können, zu glauben, dass die NIC die MAC-Adresse eines Benutzers hat. Der Vorgang des Maskierens einer MAC-Adresse wird als MAC-Spoofing bezeichnet. Im Wesentlichen geht es beim MAC-Spoofing darum, die Identität eines Computers zu ändern, egal aus welchem Grund, und es ist relativ einfach

Das Ändern der zugewiesenen MAC-Adresse kann es dem Benutzer ermöglichen, Zugriffskontrolllisten auf Servern oder Routern zu umgehen und so entweder einen Computer in einem Netzwerk zu verstecken oder sich als ein anderes Netzwerkgerät auszugeben

# IDLE / PASSIVE Scans ATTACK
Der Leerlauf-Scan ist eine TCP-Port-Scan-Methode, die darin besteht, gefälschte Pakete an einen Computer zu senden, um herauszufinden, welche Dienste verfügbar sind. Dies wird erreicht, indem man sich als ein anderer Computer ausgibt, dessen Netzwerkverkehr sehr langsam oder nicht vorhanden ist (d. h. keine Informationen sendet oder empfängt). Dies könnte ein inaktiver Computer sein, ein sogenannter "Zombie"

Diese Aktion kann über gängige Software-Netzwerkdienstprogramme wie nmap und hping durchgeführt werden. Bei dem Angriff werden gefälschte Pakete an ein bestimmtes Rechnerziel gesendet, um bestimmte Merkmale eines anderen Zombie-Rechners zu finden. Der Angriff ist raffiniert, weil es keine Interaktion zwischen dem Angreifer-Computer und dem Ziel gibt: Der Angreifer interagiert nur mit dem "Zombie"-Computer.

Dieser Exploit erfüllt zwei Zwecke: Er ist ein Port-Scanner und ein Mapper für vertrauenswürdige IP-Beziehungen zwischen Rechnern. Das Zielsystem interagiert mit dem "Zombie"-Computer, und es können Unterschiede im Verhalten beobachtet werden, indem verschiedene "Zombies" verwendet werden, die auf unterschiedliche Privilegien hinweisen, die das Ziel den verschiedenen Computern gewährt

Die allgemeine Absicht hinter dem Idle-Scan ist es, "den Port-Status zu überprüfen, während er für den Ziel-Host völlig unsichtbar bleibt.

<img src="pics/idlescan1.gif" height="auto" width="auto">
<br>
</br>
<img src="pics/idlescan2.gif" height="auto" width="auto">

# ARP Poisoning
ARP-Poisoning , auch ARP-Spoofing, ist eine Art von Cyber-Angriff, der über ein lokales Netzwerk (LAN) ausgeführt wird. Dabei werden bösartige ARP-Pakete an ein Standard-Gateway in einem LAN gesendet, um die Paarungen in seiner IP-zu-MAC-Adresstabelle zu ändern. Das ARP-Protokoll übersetzt IP-Adressen in MAC-Adressen.

<img src="pics/arp_spoofing.png" height="auto" width="auto">

# DNS Poisoning
DNS-Poisoning, auch bekannt als DNS-Cache-Poisoning oder DNS-Spoofing, ist ein hochgradig betrügerischer Cyberangriff, bei dem Hacker den Webverkehr auf gefälschte Webserver und Phishing-Websites umleiten

<img src="pics/DNSPoisoning.png" height="auto" width="auto">

# Heartbleed Attacke
Der Heartbleed-Bug ist eine schwerwiegende Schwachstelle in der beliebten kryptografischen Software-Bibliothek OpenSSL. Diese Schwachstelle ermöglicht das Stehlen von Informationen, die unter normalen Bedingungen durch die SSL/TLS-Verschlüsselung geschützt sind, die zur Sicherung des Internets verwendet wird. SSL/TLS bietet Kommunikationssicherheit und Datenschutz über das Internet für Anwendungen wie Web, E-Mail, Instant Messaging (IM) und einige virtuelle private Netzwerke (VPNs).

Der Heartbleed-Bug ermöglicht es jedem im Internet, den Speicher der Systeme zu lesen, die durch die anfälligen Versionen der OpenSSL-Software geschützt sind. Dadurch werden die geheimen Schlüssel kompromittiert, die zur Identifizierung der Dienstanbieter und zur Verschlüsselung des Datenverkehrs, der Namen und Passwörter der Benutzer sowie der eigentlichen Inhalte verwendet werden. Dies ermöglicht Angreifern, die Kommunikation abzuhören, Daten direkt von den Diensten und Benutzern zu stehlen und sich als Dienste und Benutzer auszugeben.

<img src="pics/heartbleed.png" height="auto" width="auto">

# Honeypots
Ein Honeypot ist ein Computer oder ein Computersystem, das dazu dient, wahrscheinliche Ziele von Cyberangriffen zu imitieren. Er kann verwendet werden, um Angriffe zu erkennen oder sie von einem legitimen Ziel abzulenken. ... Cyberkriminelle werden von Honeypots angezogen wie Mäuse von mit Käse gefütterten Mausefallen.

Indem Sie den Datenverkehr zu Honeypot-Systemen überwachen, können Sie besser verstehen, woher Cyberkriminelle kommen, wie sie arbeiten und was sie wollen. Noch wichtiger ist, dass Sie feststellen können, welche Ihrer Sicherheitsmaßnahmen funktionieren - und welche möglicherweise verbessert werden müssen.

<img src="pics/honeypot.jpg" height="auto" width="auto">

# Tunnelling
In Computernetzwerken ist ein Tunneling-Protokoll ein Kommunikationsprotokoll, das die Bewegung von Daten von einem Netzwerk in ein anderes ermöglicht. Es ermöglicht die Übertragung privater Netzwerkkommunikation über ein öffentliches Netzwerk (z. B. das Internet) durch einen Prozess, der als Einkapselung bezeichnet wird.

Da beim Tunneln die Verkehrsdaten in eine andere Form umverpackt werden, vielleicht mit einer standardmäßigen Verschlüsselung, kann die Art des Datenverkehrs, der durch einen Tunnel läuft, verborgen werden.

Ein Tunneling-Protokoll kann z. B. die Ausführung eines fremden Protokolls über ein Netzwerk ermöglichen, das dieses bestimmte Protokoll nicht unterstützt, z. B. die Ausführung von IPv6 über IPv4.


# Grundkonzept Datenschutz
Datensicherheit bezieht sich auf den Prozess des Schutzes von Daten vor unbefugtem Zugriff und Datenkorruption während ihres gesamten Lebenszyklus. Datensicherheit umfasst Verfahren zur Datenverschlüsselung, Hashing, Tokenisierung und Schlüsselverwaltung, die Daten über alle Anwendungen und Plattformen hinweg schützen.


    Vertraulichkeit. Stellt sicher, dass nur autorisierte Benutzer mit den richtigen Berechtigungsnachweisen auf die Daten zugreifen können.

    Integrität. Stellt sicher, dass alle gespeicherten Daten zuverlässig und genau sind und nicht unberechtigt verändert werden.

    Verfügbarkeit. Stellt sicher, dass die Daten leicht - und sicher - zugänglich und für laufende Geschäftsanforderungen verfügbar sind.

# JAP
Java Anonymous Proxy
HTTP(s) FTP

# TOR 
The Onion Routing Project

# Firewalls
Eine Firewall ist ein Netzwerksicherheitsgerät, das den ein- und ausgehenden Netzwerkverkehr überwacht und Datenpakete basierend auf einem Satz von Sicherheitsregeln zulässt oder blockiert.

# Iptables
iptables ist ein Befehlszeilen-Firewallprogramm, das Richtlinienketten verwendet, um Datenverkehr zuzulassen oder zu blockieren. Wenn eine Verbindung versucht, sich auf Ihrem System einzurichten, sucht iptables nach einer Regel in seiner Liste, um sie zu erfüllen. Wenn es keine findet, greift es auf die Standardaktion zurück

iptables verwendet drei verschiedene Ketten: Eingabe, Weiterleitung und Ausgabe.

INPUT - Diese Kette wird verwendet, um das Verhalten für eingehende Verbindungen zu steuern. Wenn z. B. ein Benutzer versucht, per SSH auf Ihren PC/Server zuzugreifen, versucht iptables, die IP-Adresse und den Port mit einer Regel in der Eingabekette abzugleichen.

FORWARD - Diese Kette wird für eingehende Verbindungen verwendet, die nicht wirklich lokal zugestellt werden. Stellen Sie sich einen Router vor - es werden immer Daten an ihn gesendet, die aber selten tatsächlich für den Router selbst bestimmt sind; die Daten werden einfach an ihr Ziel weitergeleitet. Solange Sie nicht irgendeine Art von Routing, NATing oder etwas anderes auf Ihrem System machen, das eine Weiterleitung erfordert, werden Sie diese Kette gar nicht verwenden.

OUTPUT - Diese Kette wird für ausgehende Verbindungen verwendet. Wenn Sie z. B. versuchen, howtogeek.com anzupingen, prüft iptables seine Ausgabekette, um zu sehen, welche Regeln in Bezug auf ping und howtogeek.com gelten, bevor es eine Entscheidung trifft, den Verbindungsversuch zuzulassen oder abzulehnen.

Wenn Sie Ihre Standard-Kettenrichtlinien konfiguriert haben, können Sie damit beginnen, Regeln zu iptables hinzuzufügen, damit es weiß, was zu tun ist, wenn es auf eine Verbindung von oder zu einer bestimmten IP-Adresse oder einem bestimmten Port trifft. In dieser Anleitung werden wir die drei grundlegendsten und am häufigsten verwendeten "Antworten" durchgehen.

ACCEPT - Erlaubt die Verbindung.

DROP - Beenden Sie die Verbindung, tun Sie so, als wäre sie nie passiert. Dies ist am besten, wenn Sie nicht wollen, dass die Quelle weiß, dass Ihr System existiert.

REJECT - Lässt die Verbindung nicht zu, sondern sendet eine Fehlermeldung zurück. Dies ist am besten, wenn Sie nicht wollen, dass eine bestimmte Quelle eine Verbindung zu Ihrem System herstellt, aber Sie wollen, dass sie weiß, dass Ihre Firewall sie blockiert.hat your firewall blocked them.

# Application Level Firewall
Eine Anwendungsfirewall ist eine Form der Firewall, die die Ein-/Ausgabe oder Systemaufrufe einer Anwendung oder eines Dienstes kontrolliert. Sie arbeitet, indem sie die Kommunikation auf der Grundlage einer konfigurierten Richtlinie überwacht und blockiert, in der Regel mit vordefinierten Regelsätzen, aus denen Sie wählen können. Die Anwendungs-Firewall kann die Kommunikation bis zur Anwendungsschicht des OSI-Modells kontrollieren, die die höchste Betriebsschicht darstellt und daher ihren Namen trägt. Die beiden Hauptkategorien von Application Firewalls sind netzwerkbasiert und hostbasiert.

# IDS
Ein Intrusion Detection System (IDS) ist ein Gerät oder eine Softwareanwendung, die ein Netzwerk auf bösartige Aktivitäten oder Richtlinienverstöße überwacht. Jede bösartige Aktivität oder Verletzung wird in der Regel über ein Sicherheitsinformations- und Ereignisverwaltungssystem gemeldet oder zentral gesammelt.

<img src="pics/ids.png" height="auto" width="auto">

# NIDS
Ein netzwerkbasiertes Intrusion Detection System (NIDS) erkennt bösartigen Datenverkehr in einem Netzwerk. NIDS benötigen in der Regel promiskuitiven Netzwerkzugang, um den gesamten Datenverkehr zu analysieren, einschließlich des gesamten Unicast-Verkehrs. NIDS sind passive Geräte, die nicht in den von ihnen überwachten Verkehr eingreifen; Abb.

# HIDS
Ein Host-basiertes Intrusion Detection System (HIDS) ist ein Intrusion Detection System, das in der Lage ist, die Interna eines Computersystems sowie die Netzwerkpakete an seinen Netzwerkschnittstellen zu überwachen und zu analysieren, ähnlich der Arbeitsweise eines Netzwerk-basierten Intrusion Detection Systems (NIDS).

# IPS
Intrusion prevention systems (IPS) are intrusion detection systems (IDS) that go beyond the mere generation of events to provide functions that can defend against a detected attack.


# Social Engineering
Social Engineering ist der Begriff für ein breites Spektrum bösartiger Aktivitäten, die durch menschliche Interaktionen ausgeführt werden. Es nutzt psychologische Manipulation, um Benutzer dazu zu bringen, Sicherheitsfehler zu begehen oder sensible Informationen preiszugeben. Social-Engineering-Angriffe erfolgen in einem oder mehreren Schritten.
